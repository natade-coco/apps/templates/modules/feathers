# Feathers

Feathers.jsを使用したリアルタイム対応サーバーアプリ

| Branch | Version |
| :-- | :-- |
| main | 通常版 |
| with-ds | Directusあり版 |

## 開発手順

一部の環境変数を取得するために、一度管理コンソールからアプリを登録しておく必要があります。

### 1. 環境変数ファイルを作成

`server` フォルダ直下に `.env` ファイルを作成し、以下の環境変数を設定します。

| 変数名 | 説明 | デフォルト | 備考 |
| :-- | :-- | :-- | :-- |
| NATADECOCO_ID | natadeCOCO ID | | 管理コンソールのアプリ詳細から取得 |
| NATADECOCO_SECRET | natadeCOCO Secret | | 管理コンソールのアプリ詳細から取得 |
| NODE_ENV | stg または prd | prd | |
| EXEC_ENV | local または global | global | localの場合、ユーザー認証をスキップ |

#### `.env` ファイルの例

```
NATADECOCO_ID=XXXXXXX
NATADECOCO_SECRET=XXXXXXX
NODE_ENV=stg
EXEC_ENV=local
```

### 2. 初期設定(インストール)

```
npm install
```

### 3. サーバーアプリを立ち上げる

```
npm start
```

`localhost:3030` に立ち上がります。

## Serviceの追加方法

### feathers-cliをインストール

```
npm install -g feathers-cli
```

### Serviceを追加

```
feathers generate service
```

基本的には `A custom service` を選択する。
