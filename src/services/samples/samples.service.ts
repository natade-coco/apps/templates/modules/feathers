// Initializes the `samples` service on path `/samples`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Samples } from './samples.class';
import hooks from './samples.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'samples': Samples & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/samples', new Samples(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('samples');

  service.hooks(hooks);
}
