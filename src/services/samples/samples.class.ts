import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'
import axiosRetry from 'axios-retry'

import { Application } from '../../declarations'

interface Data {
  id: string,
  name: string
}

interface ServiceOptions {}

export class Samples implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options
    this.app = app
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: 'BASE_URL'
    })
    axiosRetry(axios, {
      retries: 3
    })
    return axios
  }

  async setup(app: Application, path: string): Promise<void> {
  }

  async find(params?: Params): Promise<Data[]> {
    return [{
      id: 'sample-id',
      name: 'Sample Name'
    }]
  }

  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create(data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }
}
