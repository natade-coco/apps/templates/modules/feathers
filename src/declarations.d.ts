import { AuthenticationService } from "@feathersjs/authentication/lib";
import { Application as ExpressFeathers } from "@feathersjs/express";

import { Samples } from "./services/samples/samples.class";

// A mapping of service names to types. Will be extended in service files.
export interface ServiceTypes {
  "/authentication": AuthenticationService;
  "/samples": Samples;
}
// The application instance type that will be used everywhere else
export type Application = ExpressFeathers<ServiceTypes>;
